#!/usr/bin/python3

import argparse
import os
import sys
import inspect


import primers_generation as pg
import dna_file_reader as dfr


"""
design dna eBlocks for ordered fragments assembly

fragment a long dna sequence payload into eBlocks

add overhangs to link each eBlocks into bigBlocks

add primers to first/last eBlocks of each bigBlock

add bsaI site and buffer to each oligo_block

save files of every steps in a directory
"""


def sac_a_dos(eBlock_dicts_dict: dict, overhang_list: list):
    """
    dict {filename:dict(eBlocks)} #TODO
    """
    
    sizes_dicts = {}
    max_size = len(overhang_list)
   
    
    sacs_list = [] # list of sacs of filenames where sum of values <= max_size
    
    for filename, eBlock_dict in eBlock_dicts_dict.items():
        size_dict = len(eBlock_dict.values())
        
        new_sac = True
        for sac in sacs_list:
            if len(sac) + size_dict <= max_size:
                sac.append(filename)
                new_sac = False
                break
        if new_sac:
            sacs_list.append([filename])


def add_intern_overhangs(block_dict: dict, overhang_list: list, output_path: str = None) -> (dict, str):
    """
    add predefined overhangs at each blocks to make the intra-Block joints
          B1 + ov1
    ov1 + B2 + ov2
    ...
    ov8 + B9 + ov9
    ov9 + B10
    """
    
    block_names = list(block_dict.keys())
    
    if len(block_names) == 1: # no overhang addition for single blocks
        dfr.save_dict_to_fasta(block_dict, output_path)
        return block_dict, list(block_dict.values())[0]
        
    used_overhangs = overhang_list[:len(block_names)-1] # overhangs that will be used for this assembly
    
    overhang_block_dict = {}
    blocks_assembly_sequence = "" # sequence to simulate what the blocks assembly should be

    # first oligo_block of a bigBlock : overhang only at the end
    b_name = block_names[0]
    overhang_block_dict[b_name] = block_dict[b_name] + used_overhangs[0]
    blocks_assembly_sequence += block_dict[b_name] + used_overhangs[0]
    
    # other eBlocks : overhang at the beginning + at the end
    # the overhang at the end of oligo_block N is the same as the overhang at the beginning of oligo_block N+1
    for num_block in range(1, len(block_names)-1):
        b_name = block_names[num_block]
        overhang_block_dict[b_name] = used_overhangs[num_block-1] + block_dict[b_name] + used_overhangs[num_block]
        blocks_assembly_sequence += block_dict[b_name] + used_overhangs[num_block] # do not add the overhang at the start of the bock since it is already at the end of previous block
   
    # last fragment : overhang only at the start
    b_name = block_names[-1]
    overhang_block_dict[b_name] = used_overhangs[-1] + block_dict[b_name]
    blocks_assembly_sequence += block_dict[b_name]

    if output_path:
        dfr.save_dict_to_fasta(overhang_block_dict, output_path)

    return overhang_block_dict, blocks_assembly_sequence


def add_primers(block_dict: dict, start_primer: str, stop_primer: str, output_path: str = None) -> dict:
    """
    add the primers to the first and last eBlocks
    p1 + B1
         B10 + p2
    """
    
    primers_block_dict = {}
    primer_block_list = list(block_dict.values())

    primer_block_list[0] = start_primer + primer_block_list[0] # start primer at the beginning
    primer_block_list[-1] = primer_block_list[-1] + dfr.reverse_complement(stop_primer) # reverse complement of stop primer at the end
  
    for i, key in enumerate(block_dict.keys()):
        primers_block_dict[key] = primer_block_list[i]
    
    
    if output_path:
        dfr.save_dict_to_fasta(primers_block_dict, output_path)

    return primers_block_dict


def add_extern_overhangs(block_dict: dict, overhang_list: list, output_path: str = None) -> dict:
    """
    add overhangs at the extremity blocks
    used to create single strand part to avoid random assembly of the molecules together
    """
    #TODO choose overhangs depending on the other molecules
    first_overhang, last_overhang = overhang_list[0], overhang_list[1]
    
    block_names = list(block_dict.keys())
    
    overhang_block_dict = block_dict.copy()
    
    # overhang at the beginning of first block
    b_name = block_names[0]
    overhang_block_dict[b_name] = first_overhang + block_dict[b_name]
    
    # overhang at the end of last block
    b_name = block_names[-1]
    overhang_block_dict[b_name] = block_dict[b_name] + last_overhang

    if output_path:
        dfr.save_dict_to_fasta(overhang_block_dict, output_path)

    return overhang_block_dict
    

def add_bsaI(block_dict: dict, output_path: str = None) -> dict:
    """
    add bsaI restriction sites next to every overhangs, used in biology part to remove dna in order to make the overhangs part a single strand

    BsaI1 + B1 + bsaI2
    BsaI1 + B2 + BsaI2
    ...
    bsaI1 + B10 + BsaI1
    """
    
    start_bsaI = "GGTCTCG" # bsaI site + 1 base # goes before an overhang
    stop_bsaI = "TGAGACC" # 1 base + bsaI site # goes after an overhang
    
    bsaI_block_dict = {}
    bsaI_block_list = list(block_dict.values())

    for key, value in block_dict.items():
        bsaI_block_dict[key] = start_bsaI + value + stop_bsaI
    
    if output_path:
        dfr.save_dict_to_fasta(bsaI_block_dict, output_path)
    
    return bsaI_block_dict


def add_buffer(block_dict: str, output_path: str = None) -> dict:
    """
    add a buffering zone around each block
    just pick some payload of another block, the buffer will be removed in biology
    buff1  + B1  + buff2
    ...
    buff19 + B10 + buff20
    """
    
    buffer_size = 15
    block_list = list(block_dict.values()) # list of blocks where the buffer is taken, usually take a buffer in the N+1 block to use in the N block

    block_list.append(block_list[0]) # add the first block at the end so the last buffer can use it
    
    buffer_block_dict = {}

    # choose some payload in the next fragment
    for i, frag_name in enumerate(list(block_dict.keys())):
        mid_block_index = len(block_list[i+1])//2
        
        start_buffer = dfr.reverse_complement(block_list[i+1][mid_block_index-buffer_size:mid_block_index-1])
        stop_buffer = dfr.reverse_complement(block_list[i+1][mid_block_index:mid_block_index+buffer_size-1])
        
        #the addition of the buffer can create homopolymere
        #set a base different from extremity and start of buffer in between
        
        alphabet = [ a for a in ["A", "G", "C", "T"] if a not in [block_dict[frag_name][0], start_buffer[-1]] ]
        start_buffer += alphabet[0]
        
        alphabet = [ a for a in ["A", "G", "C", "T"] if a not in [block_dict[frag_name][-1], stop_buffer[0]] ]
        stop_buffer = alphabet[0] + stop_buffer
        
        
        buffered_block = start_buffer + block_dict[frag_name] + stop_buffer
        #TODO test for no inverse repeat region between buffer and payload
        
                    
        buffer_block_dict[frag_name] = buffered_block
        
    if output_path:
        dfr.save_dict_to_fasta(buffer_block_dict, output_path)
    
    return buffer_block_dict


def eBlocks_design(payload_fragments_dir_path: str, output_dir_path: str) -> None:
    """
    create the dna eBlocks for the ordered assembly
    """
    
    #eBlock_dict = binary_to_fragments(binary_path, output_path+"/1_fragments.fasta")
    #eBlock_dict = sequence_fragmentation(payload_path, payload_block_length, output_path+"/1_fragments.fasta")

    # get the list of overhangs, should be in the same directory of this script file
    overhang_list = list(dfr.read_fasta(os.path.abspath(os.path.dirname(__file__))+"/overhangs_eBlocks.fasta").values())
    
    full_assembly_total_sequences = [] # save the total sequences of each assembly
    total_blocks_number = 0 # number of blocks in the directory
    
    # loop over payload sequences files
    for filename in os.listdir(payload_fragments_dir_path):
        file_path = os.path.join(payload_fragments_dir_path, filename)
        # checking if it is a file
        if os.path.isfile(file_path):
            
            # create a subdir for this assembly design
            output_subdir_path = output_dir_path+"/"+filename.replace(".fasta","")
            try:
                os.mkdir(output_subdir_path)
            except OSError as error:
                print(error)
            
            # read the sequences
            eBlock_dict = dfr.read_fasta(file_path)
            total_blocks_number += len(eBlock_dict.values())
        
            # add the overhangs for each oligo_block and save it into a file
            _, blocks_assembly_sequence = add_intern_overhangs(eBlock_dict, overhang_list, output_subdir_path+"/1_blocks_overhang.fasta")
                
            # create a sequence containing the concatenation of all eBlocks with overhangs
            full_assembly_total_sequences.append(blocks_assembly_sequence)
        
        elif os.path.isdir(file_path):
            print("error oligo_block design : directory found in payload_fragments_dir_path", filename)
            exit(0)
            
    
    all_total_sequences_concat = "".join(full_assembly_total_sequences)
    print("total sequences len :",str(len(all_total_sequences_concat)))
    print("in",str(total_blocks_number),"blocks")
    
    # generate primers compatible with the full joined sequence
    compatible_primers_list = pg.generate_compatible_primers(all_total_sequences_concat)
    print(len(compatible_primers_list),"primers")
    
    # select a primers couples for each assembly
    #TODO selected_primers_list = pg.select_primers(compatible_primers_list, len(full_assembly_total_sequences))
    selected_primers_list = pg.select_primers(compatible_primers_list, 1) # only 1 couple for all storage
    selected_primers_list = selected_primers_list * len(full_assembly_total_sequences) # get a list of n times the same couple for the n molecules

    assigned_primers_dict = {} # to save which primers couple is assigned to which file
        
    if len(selected_primers_list) < 2*len(full_assembly_total_sequences): # not enough primers couple
        exit(1)
    
    primers_index = 0 # index for the primers to use in the list for each assembly
    
    # loop over oligo_block with overhangs sequences files and add remaining assembly stuff
    for filename in os.listdir(output_dir_path):
        
        # checking if it is a file
        if os.path.isfile(os.path.join(output_dir_path, filename)):
            print(filename)
            continue # then ignore it            
        
        
        output_subdir_path = os.path.join(output_dir_path, filename)
                                          
        overhang_block_dict = dfr.read_fasta(output_subdir_path+"/1_blocks_overhang.fasta") # get the dict of eBlocks with overhangs
        
        start_primer, stop_primer = selected_primers_list[primers_index], selected_primers_list[primers_index+1] # take a couple of primers to use in this assembly
        
        assigned_primers_dict["start_primer_"+filename] = start_primer
        assigned_primers_dict["stop_primer_"+filename] = stop_primer
        
        primers_index += 2

        primers_block_dict = add_primers(overhang_block_dict, start_primer, stop_primer, output_subdir_path+"/2_blocks_overhang_primers.fasta")

        extern_overhang_blocks = add_extern_overhangs(primers_block_dict, overhang_list, output_subdir_path+"/3_blocks_overhang_primers_overhangs.fasta")
        
        # add the bsaI sites to each oligo_block
        bsaI_block_dict = add_bsaI(extern_overhang_blocks, output_subdir_path+"/4_blocks_bsaI.fasta")
        
        # add the buffers to each oligo_block
        buffer_block_dict = add_buffer(bsaI_block_dict, output_subdir_path+"/5_blocks_buffer.fasta")

    dfr.save_dict_to_fasta(assigned_primers_dict, output_dir_path+"/assigned_primers.fasta") # save the selected primers


    
# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='divide a sequence into eBlocks, assemble it into bigBlocks, then groups with primers, overhangs, bsa1 sites and buffers')
    parser.add_argument('-i', action='store', dest='payload_fragments_dir_path', required=True,
                        help='path to the directory containing .fasta dna payload sequences of the eBlocks')
    parser.add_argument('-o', action='store', dest='output_dir_path', required=True,
                        help='path to the output directory that contains all steps of construction')


    # ---------- input list ---------------#
    arg = parser.parse_args()
    
    print("oligo_block design...")
    
    eBlocks_design(arg.payload_fragments_dir_path, arg.output_dir_path)

    print("\tcompleted !")

