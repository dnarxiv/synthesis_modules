
import random



def generate_random_document(n_word, doc_length):
    
    rand_document = []
    for i in range(doc_length):
        rand_word = str(random.choice(range(n_word)))
        rand_document.append(rand_word)
        
    return rand_document


def basic_assembly(document):
    assembled_document = ""
    assembly_count = 0
    for word in document:
        assembled_document += word+"_"
        assembly_count += 1
    assembled_document = assembled_document[:-1]
    print("basic assembly :",str(assembly_count),"assemblies")#,assembled_document)


def optimised_assembly(document):

    assembled_document = document
    assembly_count = 0

    def find_most_frequent_pair(document):
        """
        get the pair of consecutive words that appears the most in the document
        """
        pairs_frequency = {}
        for i in range(len(document)-1):
            word, next_word = document[i], document[i+1]
            pair = word+"+"+next_word
            pairs_frequency[pair] = pairs_frequency.get(pair, 0) + 1
        return max(pairs_frequency, key=pairs_frequency.get)
    
    def assemble_pair(document, pair):
        """
        replace the pair in the document by a word made by its assembly
        """
        [pair_word, pair_next_word] = pair.split("+")
        document_with_assembled_pair = []
        
        skip_next = False
        for i in range(len(document)-1):
            if skip_next: # arrived on a word that is the assembled next_word of a pair, so skip it
                skip_next = False
                continue
            
            word, next_word = document[i], document[i+1]
            if pair_word == word and pair_next_word == next_word:
                document_with_assembled_pair.append(word+"_"+next_word)
                skip_next = True
            else:
                document_with_assembled_pair.append(word)
        
        if not skip_next: # add the last word if not in an assembled pair
            document_with_assembled_pair.append(document[-1])
        return document_with_assembled_pair
    
    # assemble the most frequent pairs until the whole document is assembled
    while len(assembled_document) > 1:
        
        most_frequent_pair = find_most_frequent_pair(assembled_document)
        assembled_document = assemble_pair(assembled_document, most_frequent_pair)
        assembly_count += 1
        
    
    print("optimised assembly :", str(assembly_count), "assemblies")#,assembled_document[0])
    
    
# =================== main ======================= #
if __name__ == '__main__':
    
    n_word, doc_length = 10, 10000
    rand_document = generate_random_document(n_word, doc_length)
    print("\nrandom document of", str(doc_length), "with", str(n_word), "words")
    #print(rand_document)
    basic_assembly(rand_document)
    optimised_assembly(rand_document)
    
    