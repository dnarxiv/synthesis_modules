#!/usr/bin/python3

import os
import sys
import inspect
import subprocess
import random

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import sequence_control

import dna_file_reader as dfr


"""
manage primers generation and selection,

use the primer_generator script
"""

def generate_compatible_primers(sequence_between: str) -> list:
    """
    use primer_generator script to generate a lot of primers that doesn't hybridize with the given sequence
    """
    
    #___generate a lot of primers with good properties___#
    
    primer_generator_dir = currentdir+"/primer_generator"
    primers_gen_script = primer_generator_dir+"/bin/primers_gen" # path to the script
    random_sequence_path = primer_generator_dir+"/data/random_seq_1M.fasta" # some very long random sequences to create the primers from, this sequence respects the biological constraints
    primers_param_path = primer_generator_dir+"/data/param_dnarxiv.txt" # file containing the parameters for the primer_generator scripts
    output_primers_path = primer_generator_dir+"/temp/primers.fasta" # output path for generated primers
    
    # generate a file containing a lot of primers full filling the general conditions set in param_dnarxiv.txt
    primers_gen_command = primers_gen_script+' '+random_sequence_path+' '+primers_param_path+' '+output_primers_path
    subprocess.call('/bin/bash -c "$PRIMERGEN"', shell=True, env={'PRIMERGEN': primers_gen_command})
    
    
    #___select primers without hybridation with the encoded data___#
    
    sequence_between_path = primer_generator_dir+"/temp/sequence_between.fasta" # sequence representing the assembly of all encoded data with the overhangs

    dfr.save_sequence_to_fasta("sequence_between", sequence_between, sequence_between_path)
        
    primers_filter_script = primer_generator_dir+"/bin/primers_chk"
    output_compatible_primers_path = primer_generator_dir+"/temp/compatible_primers.fasta" # output path for filtered primers that doesn't hybridize with the assembly
    output_hyb = primer_generator_dir+"/temp/hyb" # list of hybridized primers (unused)
    output_dic = primer_generator_dir+"/temp/dic" # dict of primers (unused)
    output_pos = primer_generator_dir+"/temp/pos" # positions of primers (unused)
    
    # filter the primers that hybridize with the whole sequence of encoded data assembled with the overhangs
    filter_primers_command = primers_filter_script+' '+sequence_between_path+' '+output_primers_path+' '+primers_param_path \
                                    +' '+output_compatible_primers_path+' '+output_hyb+' '+output_dic+' '+output_pos
    subprocess.call('/bin/bash -c "$PRIMERFILTER"', shell=True, env={'PRIMERFILTER': filter_primers_command})
    print() # needed cause the printer go at the start of the last printed line

    primers_list = list(dfr.read_fasta(output_compatible_primers_path).values())
    
    checked_primers_list = [primer for primer in primers_list if sequence_control.sequence_check(primer)] # check for potential constraints in primers
    
    random.shuffle(checked_primers_list)
    
    with open(primer_generator_dir+"/temp/checked_primers.txt", 'w') as file:
        for primer in checked_primers_list:
            # write each item on a new line
            file.write(primer+"\n")
        
    return checked_primers_list


def test_hybridation(primer: str, selected_primers_list: list, max_hybridisation_value=4) -> bool:
    """
    test the hybridisation of the primer with a list of other primers
    """
    primer_rc = dfr.reverse_complement(primer) # get the reverse complement of the primer

    for selected_primer in selected_primers_list:

        for i in range(len(primer)-max_hybridisation_value):
            # test if any part of the primer is contained in another primer from the list (= hybridisation)
            if primer[i:i+max_hybridisation_value+1] in selected_primer:
                return False
            # test also for the reverse complement
            if primer_rc[i:i+max_hybridisation_value+1] in selected_primer:
                return False
    # return true if no hybridisation at all
    return True


def compare_GC(primer_A: str, primer_B: str) -> bool:
    """
    primers have a special temperature in PCR depending on %GC, a couple (start;stop) needs to have a close tmp, so the same %GC is better
    return true if the %GC is the same for the 2 primers
    """
    start_GC = (primer_A.count("C") + primer_A.count("G"))/(primer_A.count("A")+primer_A.count("T")) # get the %GC of the first primer
    stop_GC = (primer_B.count("C") + primer_B.count("G"))/(primer_B.count("A")+primer_B.count("T")) # get the %GC of the second primer
    return start_GC == stop_GC


def select_primers(primers_list: list, number_assembly: int) -> list:
    """
    select a list of primers (2 primers for each assembly)
    primers needs to not hybridize with each other
    """
    selected_primers_list = []
       
    for index_primer, potential_start_primer in enumerate(primers_list):
        # the primer must not hybridise with any previous selected primers
        if test_hybridation(potential_start_primer, selected_primers_list, 4):
                        
            for potential_stop_primer in primers_list[index_primer+1:]: # search a corresponding stop primer in the rest of list
                # the 2 primers needs the same %GC, and a lower risk of hybridation
                if compare_GC(potential_stop_primer, potential_start_primer) and test_hybridation(potential_stop_primer, [potential_start_primer]):
                    # the primer must not hybridise with any previous selected primers
                    if test_hybridation(potential_stop_primer, selected_primers_list, 4):
                        
                        selected_primers_list += [potential_start_primer, potential_stop_primer] # add the couple to the list
                        print(str(len(selected_primers_list)//2)+"/"+str(number_assembly))   
                        
                        # if all required primers have been selected
                        if len(selected_primers_list) == number_assembly*2:
                            #print(selected_primers_list)
                            return selected_primers_list # end the script with the result
                        
                        # the corresponding stop primer has been found
                        break # return to start primers search
    
    # not enough couple of primers found
    print("error in fragment design : not enough correct primers couple found, found",str(len(selected_primers_list)),"but wanted",str(number_assembly*2))
    print("can be solved by using a longer random sequence in primers_generation to have a longer primer list ")
    
    return selected_primers_list


