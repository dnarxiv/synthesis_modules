#!/usr/bin/python3

import os
import sys
import inspect
import subprocess
import random

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import sequence_control

import dna_file_reader as dfr


"""
manage primers generation and selection,

use the dna_storage_primer_tools script from 
"""

#TODO
#param_file_name = "param_dnarxiv_bigBlocks"
param_file_name = "param_dnarxiv_V2"


def generate_compatible_primers(primer_number: int, output_primers_path=None):
    """
    use primer_generator script to generate a lot of primers that doesn't hybridize with the given sequence
    """
    
    #___generate a lot of primers with good properties___#
    
    primer_generator_dir = currentdir+"/dna-storage-primer-tools"
    primers_gen_script = primer_generator_dir+"/bin/DSPgen" # path to the script
    primers_param_path = primer_generator_dir+"/data/"+param_file_name+".txt" # file containing the parameters for the primer_generator scripts
    if output_primers_path is None:
        output_primers_path = primer_generator_dir+"/data/temp/primers.fasta" # default output path for generated primers

    scm_option = "1" # salt/magnesium correction method: 0 = Santalucia | 1 = Owczarzy | 2 = IDT
    SEED = "0"

    # generate a file containing a lot of primers fullfilling the general conditions set in param_dnarxiv.txt
    primers_gen_command = primers_gen_script+' '+str(primer_number)+' '+primers_param_path+' '+output_primers_path+' -scm '+scm_option+' -seed '+SEED+' -dtg 1'
    subprocess.call('/bin/bash -c "$PRIMERGEN"', shell=True, env={'PRIMERGEN': primers_gen_command})
    


def filter_primers_with_oligos(oligo_path, input_primers_path=None, output_filtered_primers_path=None):
    """
    select primers without hybridation with the encoded oligos
    """

    #---call the hybridization check script---#
    primer_generator_dir = currentdir+"/dna-storage-primer-tools"
    primers_filter_script = primer_generator_dir+"/bin/DSPhyb" # path to the script
    primers_param_path = primer_generator_dir+"/data/"+param_file_name+".txt" # file containing the parameters for the primer_generator scripts
    hybridation_results = primer_generator_dir+"/data/temp/hybridations.txt" # result file listing primers with hybridizations
    if input_primers_path is None:
        input_primers_path = primer_generator_dir+"/data/temp/primers.fasta" # default path for input generated primers from generate_compatible_primers()

    if output_filtered_primers_path is None:
        output_filtered_primers_path = primer_generator_dir+"/data/temp/compatible_primers.fasta" # default path for output filtered primers that doesn't hybridize with the oligos

    filter_primers_command = primers_filter_script+' '+primers_param_path+' '+input_primers_path+' '+oligo_path+' '+hybridation_results

    subprocess.call('/bin/bash -c "$PRIMERFILTER"', shell=True, env={'PRIMERFILTER': filter_primers_command})

    #---get the list of hybrided primers from the output file---#
    with open(hybridation_results, 'r') as file:
        list_of_rejected_primers = []
        line = file.readline()
        while line != "":
            rejected_primer = int(line.split(" ")[0].replace("Primer_", ""))
            if len(list_of_rejected_primers) == 0:
                list_of_rejected_primers.append(rejected_primer)
            elif list_of_rejected_primers[-1] != rejected_primer:
                list_of_rejected_primers.append(rejected_primer)
            line = file.readline()

    primers_dict = dfr.read_fasta(input_primers_path)
    keys_list = list(primers_dict.keys())

    # delete the entries for rejected primers
    for primer_num in list_of_rejected_primers:
        del primers_dict[keys_list[primer_num]]

    checked_primers_dict = dict((name, primer) for name, primer in primers_dict.items() if sequence_control.sequence_check(primer)) # check for potential constraints in primers

    #random.shuffle(checked_primers_list)
    print(str(len(checked_primers_dict))+" primers remaining")
    # save the non hybrided primers
    dfr.save_dict_to_fasta(checked_primers_dict, output_filtered_primers_path)
        

def test_hybridation_and_GC(primer: str, selected_primers_list: list, max_hybridisation_value=4) -> bool:
    """
    test the hybridisation and GC% of the primer with a list of other primers
    return True if same GC and no hybriization found with any other primers
    """
    primer_rc = dfr.reverse_complement(primer) # get the reverse complement of the primer

    for other_primer in selected_primers_list:
        # test if the GC% is the same
        if not compare_GC(primer, other_primer):
            return False
        for i in range(len(primer)-max_hybridisation_value):
            # test if any part of the primer is contained in another primer from the list (= hybridisation)
            if primer[i:i+max_hybridisation_value+1] in other_primer:
                return False
            # test also for the reverse complement
            if primer_rc[i:i+max_hybridisation_value+1] in other_primer:
                return False

    return True


def compare_GC(primer_A: str, primer_B: str) -> bool:
    """
    primers have a special temperature in PCR depending on %GC, a couple (start;stop) needs to have a close tmp, so the same %GC is better
    return true if the %GC is the same for the 2 primers
    """
    start_GC = (primer_A.count("C") + primer_A.count("G"))/(primer_A.count("A")+primer_A.count("T")) # get the %GC of the first primer
    stop_GC = (primer_B.count("C") + primer_B.count("G"))/(primer_B.count("A")+primer_B.count("T")) # get the %GC of the second primer
    return start_GC == stop_GC


def get_pair_with_lowest_temp_diff(compatible_primers_list):
    """
    find the pair of primers with the least difference in temperature
    use list of inter compatible primers
    """

    length_of_compatibilities = [len(k) for k in compatible_primers_list]
    # get largest group of intercompatible primers
    largest_compatibility_dict = compatible_primers_list[length_of_compatibilities.index(max(length_of_compatibilities))]

    temp_dict = {} # key: primer_name; value: primer temperature
    # extract the temperature from the primers name
    for primer_name in largest_compatibility_dict.keys():
        temp_dict[primer_name] = primer_name.split(" Tm: ")[1].split(" ")[0]

    lowest_temp_diff = 1000
    best_primer_couple = "", ""
    # sort primers per temperature (ascending)
    sorted_items = sorted(temp_dict.items(), key=lambda x: x[1])

    # find the couple of primers with the least difference in temperature
    for i in range(len(sorted_items)-1):
        primer_a, temp_a = sorted_items[i]
        primer_b, temp_b = sorted_items[i+1]
        pair_temp_diff = float(temp_b) - float(temp_a)
        if pair_temp_diff < lowest_temp_diff:
            best_primer_couple = primer_a, primer_b
            lowest_temp_diff = pair_temp_diff

    if best_primer_couple == ("", ""):
        print("get_pair_with_lowest_temp_diff: no compatible primer couple found")

    return best_primer_couple


def get_pair_with_closest_temp(compatible_primers_list, other_primers_dict):
    """
    find the pair of primers with the least difference in temperature compared to the other primers
    """

    # get the medium temp of the other primers to aim
    medium_temp = sum((float(primer_name.split(" Tm: ")[1].split(" ")[0]) for primer_name in other_primers_dict.keys())) / len(other_primers_dict.keys())

    lowest_temp_diff = 1000
    best_primer_couple = "", ""

    # compare the distance of any pair temperature to the medium temperature, for evey intercompatible primers group
    for compatibility_dict in compatible_primers_list:

        temp_dict = {}  # key: primer_name; value: primer temperature
        # extract the temperature from the primers name
        for primer_name in compatibility_dict.keys():
            temp_dict[primer_name] = primer_name.split(" Tm: ")[1].split(" ")[0]

        sorted_items = sorted(temp_dict.items(), key=lambda x: x[1])
        for i in range(len(sorted_items)-1):
            primer_a, temp_a = sorted_items[i]
            primer_b, temp_b = sorted_items[i+1]
            # get difference with the medium
            pair_temp_diff = abs(medium_temp-float(temp_a)) + abs(medium_temp-float(temp_b))
            if pair_temp_diff < lowest_temp_diff:
                best_primer_couple = primer_a, primer_b
                lowest_temp_diff = pair_temp_diff


    if best_primer_couple == ("", ""):
        print("get_pair_with_closest_temp: no compatible primer couple found")
        print(compatible_primers_list)

    return best_primer_couple


def get_best_compatible_pair(source_primers_dict = None, other_primers_dict = None, only_compatible_temp = False) -> list:
    """
    select a list of primers (2 primers for each assembly)
    output primers needs to not hybridize with each other
    can add a dict of other primers that the result has to be compatible with
    param:only_compatible_temp if you don't need to check hybridation & GC with the others primers
    """

    primer_generator_dir = currentdir+"/dna-storage-primer-tools"

    if source_primers_dict is None:
        source_primers_dict = dfr.read_fasta(primer_generator_dir+"/data/temp/compatible_primers.fasta") # default input path
    if other_primers_dict is None:
        other_primers_dict = {}

    compatible_primers_list = [] # list of intercompatible primers groups found

    primers_keys = [] # list of primers names that do not hybridize with any of the other previous primers
    # primers_keys = [primer_name for primer_name in source_primers_dict.keys if primer_name not in other_primers_dict.keys()]


    for primer_name, primer_seq in source_primers_dict.items():
        if test_hybridation_and_GC(primer_seq, list(other_primers_dict.values()), 5) or only_compatible_temp:
            primers_keys.append(primer_name)

    if not primers_keys:
        print("no compatible primer couple without hybridation found")

    # get all group of primers with no inter hybridization
    for index_primer, primer_name in enumerate(primers_keys):
        primer_sequence = source_primers_dict[primer_name]

        compatible_primers_dict = {primer_name: primer_sequence} # init a group of inter compatible primers

        for other_primer_name in primers_keys[index_primer+1:]: # search a corresponding stop primer in the rest of list
            other_primer_sequence = source_primers_dict[other_primer_name]

            # the second primer needs the same %GC, and no hybridization with the group
            if test_hybridation_and_GC(other_primer_sequence, list(compatible_primers_dict.values())):
                compatible_primers_dict[other_primer_name] = other_primer_sequence

        if len(compatible_primers_dict) >= 2: # keep groups of at least 2 primers
            compatible_primers_list.append(compatible_primers_dict)

    print("compatibles lists lengths : ")
    print([len(k.keys()) for k in compatible_primers_list])
    if other_primers_dict == {}:
        # get the pair with the closest temperature
        best_primer_couple = get_pair_with_lowest_temp_diff(compatible_primers_list)

    else:
        # get the pair with the closest temperature to the other selected primers
        best_primer_couple = get_pair_with_closest_temp(compatible_primers_list, other_primers_dict)

    print(best_primer_couple)
    selected_primers_dict = {best_primer_couple[0]:source_primers_dict[best_primer_couple[0]], best_primer_couple[1]:source_primers_dict[best_primer_couple[1]]}

    return selected_primers_dict


